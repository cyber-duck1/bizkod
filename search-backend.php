<?php

include 'db.config.php';

// Ellenőrizzük, hogy a kapcsolat rendben van-e
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}

// A form adatainak lekérése
$location = $_POST['location'];
$type = $_POST['type'];
$floorArea = $_POST['floor_area'];
$bedrooms = $_POST['bedrooms'];
$gender = $_POST['gender'];
$movingInRoomer = $_POST['moving_in_roomer'];

// Alap SQL lekérdezés
$sql = "SELECT * FROM properties WHERE 1 = 1";

// Szűrési feltételek hozzáadása
$conditions = [];
$params = [];
$types = '';

// Szűrési feltételek hozzáadása, ha vannak
if (!empty($location)) {
$sql .= " AND location = '$location'";
}
if (!empty($type)) {
$sql .= " AND type = '$type'";
}
if (!empty($floorArea)) {
$sql .= " AND floor_area <= '$floorArea'";
}
if (!empty($bedrooms)) {
$sql .= " AND bedrooms = '$bedrooms'";
}
if (!empty($gender)) {
$sql .= " AND gender = '$gender'";
}
if (!empty($movingInRoomer)) {
$sql .= " AND moving_in_roomer = '$movingInRoomer'";
}
// Előkészített utasítás létrehozása
$stmt = $connection->prepare($sql);

// Paraméterek kötése
if ($stmt && !empty($types)) {
    $stmt->bind_param($types, ...$params);
}
// Lekérdezés futtatása
if ($stmt->execute()) {
    $result = $stmt->get_result();

    // Eredmények feldolgozása
    while ($row = $result->fetch_assoc()) {
        // Itt dolgozzuk fel az egyes sorokat
    }
} else {
    echo "Error: " . $stmt->error;
}

// Kapcsolat bezárása
$stmt->close();
$connection->close();
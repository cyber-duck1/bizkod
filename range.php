<?php
include("db.config.php");

$sql = "SELECT MIN(`square_meter`) AS minVal, MAX(`square_meter`) AS maxVal FROM `apartment`";
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));


if ($result->num_rows > 0) {

    $row = $result->fetch_assoc();
    $minVal = $row["minVal"];
    $maxVal = $row["maxVal"];
    echo $minVal;

} else {
    echo "0 results";
}

mysqli_close($connection);


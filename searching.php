<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

      <title>Bizkod</title>

    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

    <link rel="stylesheet" href="assets/css/style.css">

    </head>

  <script>
      document.addEventListener('DOMContentLoaded', function () {
          var checkbox = document.getElementById('toggleCheckbox');
          var normalForm = document.getElementById('normalForm');
          var specialForm = document.getElementById('specialForm');

          checkbox.addEventListener('change', function() {
              if(this.checked) {
                  specialForm.style.display = 'block';
                  normalForm.style.display = 'none';
              } else {
                  normalForm.style.display = 'block';
                  specialForm.style.display = 'none';
              }
          });
      });
  </script>




    <body>

    <!-- ***** Preloader Start ***** -->
    <div id="js-preloader" class="js-preloader">
      <div class="preloader-inner">
        <span class="dot"></span>
        <div class="dots">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </div>
    <!-- ***** Preloader End ***** -->


    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="index.php" class="logo">Real Estate<em> Website</em></a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="searching.php" class="active">Search</a></li>
                            <li><a href="testimonials.php ">Ratings</a></li>
                            <li><a href="#"><i class="bi bi-person-fill fa-2x"></i></a>
                            </li>
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Call to Action Start ***** -->
    <section class="section section-bg" id="call-to-action" style="background-image: url(assets/images/banner-image-1-1920x500.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="cta-content">
                        <br>
                        <br>
                        <h2>Our <em>Properties</em></h2>
                        <p>Ut consectetur, metus sit amet aliquet placerat, enim est ultricies ligula</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Call to Action End ***** -->

    <!-- ***** Fleet Starts ***** -->

                <?php
                include ("db.config.php");
                $query = "SELECT * FROM `user`";

                // Ellenőrzi, hogy helyes-e a $connection és $query
                $result = mysqli_query($connection, $query) or die(mysqli_error($connection));

                // Van-e adat a $result-ban
                if (mysqli_num_rows($result) > 0) {
                    // Lekér egy eredménysort asszociatív tömbként, numerikus tömbként vagy mindkettőként, mindaddig, amíg van adat
                    while ($record = mysqli_fetch_array($result)) {
                        // Asszociatív tömb használata a könnyebb olvashatóságért
                        $imagePath = "assets/images/" . $record['image']; // Győződj meg róla, hogy az elérési út helyes
                        $image = "<img width='200px' src='$imagePath' alt='image'>"; // A 'src' attribútumban az elérési út javítva
                        // A record['phone'] hivatkozás javítása a konszisztencia érdekében
                        echo "<div class='record_img'>$image</div> <div class='record'><b>{$record['phone_num']}</b><br></div><br>";
                    }
                    // $result felszabadítása, ami a fetch által használt memóriát szabadítja fel (etikus)
                    mysqli_free_result($result);
                } else {
                    echo "Nincs adat az adatbázisban!";
                }
                mysqli_close($connection);
                ?>


                <!--            <div class="row">-->
<!--                <div class="col-lg-4">-->
<!--                    <div class="trainer-item">-->
<!--                        <div class="image-thumb">-->
<!--                            <img src="assets/images/profile.png" alt="">-->
<!--                        </div>-->
<!--                        <div class="down-content">-->
<!--                            <span>-->
<!--                                <sup>€</sup>price<sup>/</sup>month-->
<!--                            </span>-->

<!--                            <h4>John Doe</h4>-->

<!--                            <p>location &nbsp;/&nbsp; ?moving in? &nbsp;/&nbsp; area /&nbsp;  bedrooms</p>-->

<!--                            <ul class="social-icons">-->
<!--                                <li><a href="property-details.html">+ View More</a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-4">-->
<!--                    <div class="trainer-item">-->
<!--                        <div class="image-thumb">-->
<!--                            <img src="assets/images/profile.png" alt="">-->
<!--                        </div>-->
<!--                        <div class="down-content">-->
<!--                            <span>-->
<!--                                <sup>€</sup>price<sup>/</sup>month-->
<!--                            </span>-->

<!--                            <h4>John Doe</h4>-->

<!--                            <p>location &nbsp;/&nbsp; ?moving in? &nbsp;/&nbsp; area /&nbsp;  bedrooms</p>-->

<!--                            <ul class="social-icons">-->
<!--                                <li><a href="property-details.html">+ View More</a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-4">-->
<!--                    <div class="trainer-item">-->
<!--                        <div class="image-thumb">-->
<!--                            <img src="assets/images/profile.png" alt="">-->
<!--                        </div>-->
<!--                        <div class="down-content">-->
<!--                            <span>-->
<!--                                <sup>€</sup>price<sup>/</sup>month-->
<!--                            </span>-->

<!--                            <h4>John Doe</h4>-->

<!--                            <p>location &nbsp;/&nbsp; ?moving in? &nbsp;/&nbsp; area /&nbsp;  bedrooms</p>-->

<!--                            <ul class="social-icons">-->
<!--                                <li><a href="property-details.html">+ View More</a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

<!--                <div class="col-lg-4">-->
<!--                    <div class="trainer-item">-->
<!--                        <div class="image-thumb">-->
<!--                            <img src="assets/images/profile.png" alt="">-->
<!--                        </div>-->
<!--                        <div class="down-content">-->
<!--                            <span>-->
<!--                                <sup>€</sup>price<sup>/</sup>month-->
<!--                            </span>-->

<!--                            <h4>John Doe</h4>-->

<!--                            <p>location &nbsp;/&nbsp; ?moving in? &nbsp;/&nbsp; area /&nbsp;  bedrooms</p>-->

<!--                            <ul class="social-icons">-->
<!--                                <li><a href="property-details.html">+ View More</a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-4">-->
<!--                    <div class="trainer-item">-->
<!--                        <div class="image-thumb">-->
<!--                            <img src="assets/images/profile.png" alt="">-->
<!--                        </div>-->
<!--                        <div class="down-content">-->
<!--                            <span>-->
<!--                                <sup>€</sup>price<sup>/</sup>month-->
<!--                            </span>-->

<!--                            <h4>John Doe</h4>-->

<!--                            <p>location &nbsp;/&nbsp; ?moving in? &nbsp;/&nbsp; area /&nbsp;  bedrooms</p>-->

<!--                            <ul class="social-icons">-->
<!--                                <li><a href="property-details.html">+ View More</a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-4">-->
<!--                    <div class="trainer-item">-->
<!--                        <div class="image-thumb">-->
<!--                            <img src="assets/images/profile.png" alt="">-->
<!--                        </div>-->
<!--                        <div class="down-content">-->
<!--                            <span>-->
<!--                                <sup>€</sup>price<sup>/</sup>month-->
<!--                            </span>-->

<!--                            <h4>John Doe</h4>-->

<!--                            <p>location &nbsp;/&nbsp; ?moving in? &nbsp;/&nbsp; area /&nbsp;  bedrooms</p>-->

<!--                            <ul class="social-icons">-->
<!--                                <li><a href="property-details.html">+ View More</a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>

            <br>



        </div>
    </section>
    <!-- ***** Fleet Ends ***** -->


    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p>
                        Copyright © 2024 BizKod | Cyber-Duck
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script>
    <script src="assets/js/mixitup.js"></script>
    <script src="assets/js/accordions.js"></script>

    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>
    </body>
</html>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
    <title>Bizkod</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
    
    <!-- ***** Preloader Start ***** -->
    <div id="js-preloader" class="js-preloader">
      <div class="preloader-inner">
        <span class="dot"></span>
        <div class="dots">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </div>
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="index.php" class="logo">Cim<em>der</em></a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li><a href="index.php" class="active">Home</a></li>
                            <li><a href="searching.php " >Search</a></li>
                            <li><a href="testimonials.php ">Ratings</a></li>
                            <li><a href="login.php"><i class="bi bi-person-fill fa-2x"></i></a>
                            </li>
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Main Banner Area Start ***** -->
    <div class="main-banner" id="top">
        <video autoplay muted loop id="bg-video">
            <source src="assets/images/video.mp4" type="video/mp4" />
        </video>

        <div class="video-overlay header-text">
            <div class="caption">
                <h2>Find the perfect <em>Roommate</em></h2>
                <div class="main-button">
                    <a href="registe.php">Registration</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Main Banner Area End ***** -->

   <!-- ***** Cars Starts ***** -->
    <section class="section" id="trainers">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="section-heading">
                        <h2>Comfortable home,reliable<em> roommates.</em></h2>
<p>Find your ideal roommate quickly and easily!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="trainer-item">
                        <div class="image-thumb col-lg-3">
                            <img src="assets/images/profile1.jpg" alt="" id="pic0">
                        </div>
                        <div class="down-content">
                            <span>
                                <sup>€</sup>100<sup>/</sup>month
                            </span>

                            <h4>Lucy Chen</h4>

                            <p>Subotica / Moving In/&nbsp; 100m<sup>2</sup> /&nbsp; 2 bedrooms</p>

                            <ul class="social-icons">
                                <li><a href="property-details.php">+ View More</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="trainer-item">
                        <div class="image-thumb col-lg-3">
                            <img src="assets/images/profile2.jpg" alt="" id="pic1">
                        </div>
                        <div class="down-content">
                            <span>
                                <sup>€</sup>250<sup>/</sup>month
                            </span>

                            <h4>John Smith</h4>

                            <p>Novi Sad &nbsp;/&nbsp; Roomer &nbsp;/&nbsp; 220m<sup>2</sup> /&nbsp; 3 bedrooms</p>

                            <ul class="social-icons">
                                <li><a href="property-details1.php">+ View More</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="trainer-item">
                        <div class="image-thumb col-lg-3">
                            <img src="assets/images/profile3.jpg" alt="" id="pic2">
                        </div>
                        <div class="down-content">
                            <span>
                                <sup>€</sup>235<sup>/</sup>month
                            </span>

                            <h4>Tim Bredford</h4>

                            <p>Beograd &nbsp;/&nbsp; Moving in &nbsp;/&nbsp; 197m<sup>2</sup> /&nbsp; 4 bedrooms</p>

                            <ul class="social-icons">
                                <li><a href="property-details2.php">+ View More</a></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                <div class="col-lg-3">
                    <div class="trainer-item">
                        <div class="image-thumb col-lg-3">
                            <img src="assets/images/profile4.jpg" alt="" id="pic2">
                        </div>
                        <div class="down-content">
                            <span>
                                <sup>€</sup>150<sup>/</sup>month
                            </span>

                            <h4>Lilly Smith</h4>

                            <p>Kragujevac &nbsp;/&nbsp; Roomer &nbsp;/&nbsp; 120m<sup>2</sup> /&nbsp; 1 bedrooms</p>

                            <ul class="social-icons">
                                <li><a href="property-details3.php">+ View More</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>

            <br>

            <div class="main-button text-center">
                <a href="search.php">Search</a>
            </div>
    </section>
    <!-- ***** Cars Ends ***** -->
<br><br>
    <section class="section section-bg" id="schedule" style="background-image: url(assets/images/about-fullscreen-1-1920x700.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="section-heading dark-bg">
                        <h2>Read <em>About Us</em></h2>
                        <p>Discover your perfect living match with ease and confidence.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="cta-content text-center">
<p>Our Mission: Connecting you with compatible roommates to create your ideal
    home.</p>
<p>Why Choose Us: Effortless matching and a secure community for a
    harmonious living experience.</p>
                    <p>
                        Get Started: Join us today and take the first step towards a better shared
                        living space!
                    </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ***** Blog Start ***** -->
    <section class="section" id="our-classes">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="section-heading">
                        <h2>Looking for a <em>Roommate</em>?</h2>
                    </div>
                </div>
            </div>
            <div class="row" id="tabs">
              <div class="col-lg-4">
                <ul>
                  <li>
                      <a href='#tabs-1'>Our platform is bustling with numerous potential roommates, ensuring you
                          have a wide selection to find the perfect match. Dive into a community where
                          finding the right person to share your space is just a few clicks away.</a></li>
                </ul>
              </div>
                <div class="col-lg-4">
                    <ul>
                    <li><a href='#tabs-2'>Enjoy the perks of shared living with our budget-friendly options. Find a
                        roommate that fits your financial needs quickly and easily.</a></li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <ul>
                    <li><a href='#tabs-3'>Our user-friendly platform is designed for speed, allowing you to swiftly find
                        and connect with a compatible roommate. Say goodbye to long searches; with
                        our efficient service, you'll be settling in with your new roommate in no time.</a></li>
                    </ul>
                </div>
              </div>
            </div>
    </section>
    <!-- ***** Blog End ***** -->

    <!-- ***** Call to Action Start ***** -->
    <section class="section section-bg" id="call-to-action" style="background-image: url(assets/images/banner-image-1-1920x500.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="cta-content">
                        <h2>Find your perfect <em>roommate</em></h2>
                        <p>Register to access a vast network of potential roommates, ensuring you find
                            the ideal match for your living preferences and lifestyle.</p>
                        <div class="main-button">
                            <a href="registe.php">Registration</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Call to Action End ***** -->

    <!-- ***** Testimonials Item Start ***** -->
    <section class="section" id="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="section-heading">
                        <h2>Read our <em>Ratings</em></h2>
                        <p>Leave and check ratings to ensure transparency and trustworthiness in your
                            search for the ideal roommate.</p></div>
                </div>
                <div class="col-lg-6">
                    <ul class="features-items">
                        <li class="feature-item">
                            <div class="left-icon">
                            <i class="bi bi-person-fill fa-5x"></i>
                            </div>
                            <div class="right-content">
                                <h4>John Doe</h4>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <p><em>" Found my roommate in just a week—amazing service! "</em></p>
                            </div>
                        </li>
                        <li class="feature-item">
                            <div class="left-icon">
                                <i class="bi bi-person-fill fa-5x"></i>
                            </div>
                            <div class="right-content">
                                <h4>John Doe</h4>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <p><em>" The matching system is spot on; my new roommate is a perfect fit. "</em></p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <ul class="features-items">
                        <li class="feature-item">
                            <div class="left-icon">
                                <i class="bi bi-person-fill fa-5x"></i>
                            </div>
                            <div class="right-content">
                                <h4>John Doe</h4>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star "></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <p><em>" Affordable and efficient, couldn't ask for more. "</em></p>
                            </div>
                        </li>
                        <li class="feature-item">
                            <div class="left-icon">
                                <i class="bi bi-person-fill fa-5x"></i>
                            </div>
                            <div class="right-content">
                                <h4>John Doe</h4>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <p><em>" Great experience! Safe and user-friendly platform."</em></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <br>

            <div class="main-button text-center">
                <a href="testimonials.php">Read More</a>
            </div>
        </div>
    </section>
    <!-- ***** Testimonials Item End ***** -->

    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p>
                        Copyright © 2024 BizKod | Cyber-Duck
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    <script src="assets/js/mixitup.js"></script> 
    <script src="assets/js/accordions.js"></script>
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

  </body>
</html>
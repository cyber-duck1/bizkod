<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
    <title>Bizkod</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>

<!-- ***** Preloader Start ***** -->
<div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
        <span class="dot"></span>
        <div class="dots">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</div>
<!-- ***** Preloader End ***** -->


<!-- ***** Header Area Start ***** -->
<header class="header-area header-sticky">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="main-nav">
                    <!-- ***** Logo Start ***** -->
                    <a href="index.php" class="logo">Cim<em>der</em></a>
                    <!-- ***** Logo End ***** -->
                    <!-- ***** Menu Start ***** -->
                    <ul class="nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="search.php " >Search</a></li>
                        <li><a href="testimonials.php " class="active">Ratings</a></li>
                        <li><a href="login.php"><i class="bi bi-person-fill fa-2x"></i></a>
                        </li>
                    </ul>

                    <a class='menu-trigger'>
                        <span>Menu</span>
                    </a>
                    <!-- ***** Menu End ***** -->
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->

<!-- ***** Call to Action Start ***** -->
<section class="section section-bg" id="call-to-action" style="background-image: url(assets/images/banner-image-1-1920x500.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="cta-content">
                    <br>
                    <br>
                    <sup>€</sup>150<sup>/</sup>month
                    <p>Kragujevac &nbsp;/&nbsp; Roomer &nbsp;/&nbsp; 120m<sup>2</sup> /&nbsp; 1 bedrooms</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Call to Action End ***** -->

<!-- ***** Fleet Starts ***** -->
<section class="section" id="trainers">
    <div class="container">
        <br><br>
        <div id="carouselExampleIndicators" class="carousel slide">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="assets/images/house2.jpg.jpg" alt="First slide">
                </div>
            </div>
        </div>

        <br><br>

        <div class="row" id="tabs">
            <div class="col-lg-4">
                <ul>
                    <li><a href='#tabs-1'><i class="fa fa-cog"></i>Flat Description</a></li>
                    <li><a href='#tabs-3'><i class="fa fa-phone"></i> Contact Details</a></li>
                </ul>
            </div>
            <div class="col-lg-8">
                <section class='tabs-content' style="width: 100%;">
                    <article id='tabs-1'>
                        <h4>Specification</h4>

                        <div class="row">
                            <div class="col-sm-6">
                                <label>Type</label>

                                <p>House</p>
                            </div>

                            <div class="col-sm-6">
                                <label>Bedroom</label>

                                <p>2</p>
                            </div>

                            <div class="col-sm-6">
                                <label>Floor area</label>

                                <p>135m<sup>2</sup></p>
                            </div>

                            <div class="col-sm-6">
                                <label>City</label>

                                <p>Novi Beograd</p>
                            </div>
                        </div>
                    </article>


                    <article id='tabs-3'>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Name</label>

                                <p>Lilly Smith</p>
                            </div>
                            <div class="col-sm-6">
                                <label>Email</label>
                                <p><a href="#">lillysmith@example.com</a></p>
                            </div>
                        </div>

                        <img src="assets/images/map.jpg" class="img-fluid" alt="">
                    </article>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- ***** Fleet Ends ***** -->

<!-- ***** Footer Start ***** -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p>
                    Copyright © 2024 BizKod | Cyber-Duck</a>
                </p>
            </div>
        </div>
    </div>
</footer>


<!-- jQuery -->
<script src="assets/js/jquery-2.1.0.min.js"></script>

<!-- Bootstrap -->
<script src="assets/js/popper.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<!-- Plugins -->
<script src="assets/js/scrollreveal.min.js"></script>
<script src="assets/js/waypoints.min.js"></script>
<script src="assets/js/jquery.counterup.min.js"></script>
<script src="assets/js/imgfix.min.js"></script>
<script src="assets/js/mixitup.js"></script>
<script src="assets/js/accordions.js"></script>

<!-- Global Init -->
<script src="assets/js/custom.js"></script>

</body>
</html>